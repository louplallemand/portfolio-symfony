<?php

namespace App\Entity;

use App\Repository\ProjectRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ProjectRepository::class)]
class Project
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $title;

    #[ORM\Column(type: 'text')]
    private $content;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $imgUrl1;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $imgUrl2;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $lienRepo;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $lienSite;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getImgUrl1(): ?string
    {
        return $this->imgUrl1;
    }

    public function setImgUrl1(?string $imgUrl1): self
    {
        $this->imgUrl1 = $imgUrl1;

        return $this;
    }

    public function getImgUrl2(): ?string
    {
        return $this->imgUrl2;
    }

    public function setImgUrl2(?string $imgUrl2): self
    {
        $this->imgUrl2 = $imgUrl2;

        return $this;
    }

    public function getLienRepo(): ?string
    {
        return $this->lienRepo;
    }

    public function setLienRepo(?string $lienRepo): self
    {
        $this->lienRepo = $lienRepo;

        return $this;
    }

    public function getLienSite(): ?string
    {
        return $this->lienSite;
    }

    public function setLienSite(?string $lienSite): self
    {
        $this->lienSite = $lienSite;

        return $this;
    }
}
