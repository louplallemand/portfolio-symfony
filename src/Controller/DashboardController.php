<?php

namespace App\Controller;

use App\Entity\Project;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;

class DashboardController extends AbstractController
{
    #[Route('/dashboard', name: 'app_dashboard')]
    public function index(ManagerRegistry $doctrine): Response
    {
        $request = Request::createFromGlobals();

        if ($request->request->get('title') != null){
            $entityManager = $doctrine->getManager();
            $new = new Project();
            $new->setTitle($request->request->get('title')); 
            $new->setContent($request->request->get('content'));
            $new->setImgUrl1($request->request->get('imgUrl1')); 
            $new->setImgUrl2($request->request->get('imgUrl2')); 
            $new->setLienRepo($request->request->get('lienRepo')); 
            $new->setLienSite($request->request->get('lienSite')); 
            $entityManager->persist($new);
            $entityManager->flush();
        }
        
        return $this->render('dashboard/index.html.twig');
    }
}
